# Set the base image
# Use an official Python runtime as a parent image
FROM   docker.io/centos
# Set the base image
# Dockerfile author / maintainer 
MAINTAINER Peri <docker@dsmouse.com> 

## Set the working directory to /app
#WORKDIR /config

#VOLUME [ "/data/conf.d" ]


# Copy the current directory contents into the container at /app
ADD [ "setup.sh", "setup.sh" ]
ADD [ "start.sh", "start.sh" ]

RUN /setup.sh

# Define environment variable
#ENV NAME World

ENTRYPOINT /start.sh

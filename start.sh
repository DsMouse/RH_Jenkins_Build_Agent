#!/bin/sh

java -version || rpm -ivh /java.rpm

curl ${urlbase}/jnlpJars/agent.jar -k > agent.jar

java -jar agent.jar -jnlpUrl ${urlbase}/computer/${agentname}/slave-agent.jnlp -secret ${secret}  -workDir "/Jenkins"
